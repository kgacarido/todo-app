import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { TodoService } from './todo.service';

describe('TodoService', () => {
  let service: TodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule,]
    });
    service = TestBed.inject(TodoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return completed todos', () => {
    expect(true).toBeTruthy();
  });

  it('should return active todos', () => {
    expect(true).toBeTruthy();
  });

  it('should get a todo item', () => {
    expect(true).toBeTruthy();
  });

  it('should create a todo item', () => {
    expect(true).toBeTruthy();
  });

  it('should update a todo item', () => {
    expect(true).toBeTruthy();
  });

  it('should delete a todo item', () => {
    expect(true).toBeTruthy();
  });

  it('should complete a todo item', () => {
    expect(true).toBeTruthy();
  });
});
