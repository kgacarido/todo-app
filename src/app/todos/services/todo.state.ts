import { Todo } from "../models/todo.model";

export interface TodoState {
    hasChanges?: boolean,
    isLoading?: boolean,
    isSuccess?: boolean,
}
export interface TodoStore {
    list?: Todo[],
    item?: Todo,
}

export enum TodoStatus {
    ACTIVE = 'status_active',
    DONE = 'status_done',
}