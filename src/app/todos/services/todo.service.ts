import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, finalize, map, skip, skipWhile, take, tap } from 'rxjs/operators';
import { Todo } from '../models/todo.model';
import { TodoState, TodoStatus } from './todo.state';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private readonly HEADERS = { 'content-type': 'application/json', 'dataType': 'json' };
  modifiedDate$ = new BehaviorSubject<string>('');
  todoList$ = new BehaviorSubject<Todo[]>([]);

  constructor(private http: HttpClient) {
    setInterval(() => this.fetchModified(), 15000);
    this.modifiedDate$.pipe(
      distinctUntilChanged(),
    ).subscribe(
      () => this.fetchTodoList()
    );
  }

  getFilteredTodoList(filter: string): Observable<Todo[]> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- getTodoList');
    return this.todoList$.pipe(
      distinctUntilChanged(),
      //tap(list => console.log(list)),
      map((list) => list.filter((item) => item.status == filter))
    )
  }
  getTodoItem(id: string): Observable<Todo> {
    // const transDate = new Date().toISOString();
    // console.log(transDate, '- fetchTodoList');
    // return this.http.get<Todo>('http://localhost:3000/todos/'+id);
    const transDate = new Date().toISOString();
    console.log(transDate, '- getTodoItem');
    return this.todoList$.pipe(
      tap(item => console.log('item', item)),
      map((list) => list.filter((item) => item.id == id)[0]),
    );
  }

  setModified(modifiedDate: string): void {
    const transDate = new Date().toISOString();
    if (this.modifiedDate$.value != modifiedDate) {
      console.log(transDate, '- setModified', modifiedDate);
      this.modifiedDate$.next(modifiedDate);
    }
  }
  setTodoList(todoList: Todo[]) {
    const transDate = new Date().toISOString();
    console.log(transDate, '- setTodoList', todoList);
    this.todoList$.next(todoList);
  }
  createToDo(todo: Todo): Observable<boolean> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- createToDo', todo);
    return this.http.post<boolean>('http://localhost:3000/todos',
      {
        ...todo,
        targetDate: todo.targetDate?.toISOString(),
        status: TodoStatus.ACTIVE,
        id: this.todoList$.value.length + 1,
      },
      {
        headers: this.HEADERS,
        observe: 'response',
      },
    ).pipe(
      map((success) => success.ok),
      tap(() => this.updateModified(transDate)),
    );
  }
  updateToDo(todo: Todo): Observable<boolean> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- updateToDo');
    return this.http.patch<boolean>('http://localhost:3000/todos/' + todo.id,
      {
        name: todo.name,
        targetDate: todo.targetDate,
        description: todo.description
      },
      {
        headers: this.HEADERS,
        observe: 'response',
      },
    ).pipe(
      map((success) => success.ok),
      tap(() => this.updateModified(transDate)),
    );
  }
  completeToDoStatus(id: string): Observable<boolean> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- updateToDoStatus');
    return this.http.patch<boolean>('http://localhost:3000/todos/' + id,
      { status: TodoStatus.DONE },
      { observe: 'response' },
    ).pipe(
      map((success) => success.ok),
      tap(() => this.updateModified(transDate)),
    );
  }
  activateToDoStatus(id: string): Observable<boolean> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- updateToDoStatus');
    return this.http.patch<boolean>('http://localhost:3000/todos/' + id,
      { status: TodoStatus.ACTIVE },
      { observe: 'response' },
    ).pipe(
      map((success) => success.ok),
      tap(() => this.updateModified(transDate)),
    );
  }
  deleteToDo(id: string): Observable<boolean> {
    const transDate = new Date().toISOString();
    console.log(transDate, '- deleteToDo');
    return this.http.delete('http://localhost:3000/todos/' + id, { observe: 'response' }).pipe(
      debounceTime(1000),
      map((success) => success.ok),
      tap(() => this.updateModified(transDate)),
    );
  }

  private fetchTodoList(): void {
    const transDate = new Date().toISOString();
    console.log(transDate, '- fetchTodoList');
    this.http.get<Todo[]>('http://localhost:3000/todos').subscribe(
      (next) => this.setTodoList(next)
    );
  }
  //This code runs after
  private fetchModified(): void {
    const transDate = new Date().toISOString();
    console.log(transDate, '- fetchModified');
    this.http.get<any>('http://localhost:3000/states/modified').pipe(
      //tap(response => console.log('response:', response)),
      tap(response => this.setModified(response.value))
    ).subscribe();
  }
  private updateModified(transDate: string): void {
    console.log(transDate, '- updateModified');
    this.http.put<any>('http://localhost:3000/states/modified',
      { value: transDate },
      { observe: 'response' },
    ).pipe(
      filter((success) => success.ok),
      tap(() => this.setModified(transDate))
    ).subscribe();
  }
}