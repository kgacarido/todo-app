import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todo-view',
  templateUrl: './todo-view.component.html',
  styleUrls: ['./todo-view.component.scss'],
  providers: [TodoService],
})
export class TodoViewComponent implements OnInit {
  readonly reactiveMainForm: FormGroup = new FormGroup({
    name: new FormControl(),
    targetDate: new FormControl(),
    description: new FormControl(),
  });
  private id: string | null = null;
  constructor(
    private service: TodoService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.service.getTodoItem(this.id!).subscribe(
      (next) => this.reactiveMainForm.setValue({
        description: next.description,
        targetDate: next.targetDate,
        name: next.name,
      })
    )
  }
  goBack() { }
}
