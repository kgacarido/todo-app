import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.scss'],
  providers: [TodoService],
})
export class TodoAddComponent {
  readonly reactiveMainForm: FormGroup = new FormGroup({
    name: new FormControl(undefined, [
      Validators.required,
      Validators.minLength(5),
    ]),
    targetDate: new FormControl(),
    description: new FormControl(),
  });
  readonly nameControl: FormControl = this.reactiveMainForm.get(
    'name'
  ) as FormControl;
  
  constructor(private service: TodoService) { }

  onSubmit(): void {
    this.service
      .createToDo(this.reactiveMainForm.getRawValue())
      .pipe(take(1))
      .subscribe();
  }

  onClear(): void {
    console.log('onClear');
    this.reactiveMainForm.reset();
  }
}
