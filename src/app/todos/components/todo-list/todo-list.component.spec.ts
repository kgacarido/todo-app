import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoStatus } from '../../services/todo.state';

import { TodoListComponent } from './todo-list.component';

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TodoListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should monitor filterState', () => {
    component.toggleFilter(TodoStatus.ACTIVE);
    expect(component.filterState$).toHaveBeenCalledWith();
  });
  it('should monitor loading', () => {
    component.setLoading(true);
    expect(component.loadingState$).toHaveBeenCalledWith();
  });
  it('should monitor listData', () => {
    component.setTodoList([]);
    expect(component.todoList$).toHaveBeenCalledWith();
  });
});