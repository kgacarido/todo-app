import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, take, takeLast, tap } from 'rxjs/operators';
import { Todo } from '../../models/todo.model';
import { TodoService } from '../../services/todo.service';
import { TodoStatus } from '../../services/todo.state';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  providers: [TodoService],
})
export class TodoListComponent {
  readonly todoStatus = Object.keys(TodoStatus);
  filterState$ = new BehaviorSubject<TodoStatus>(TodoStatus.ACTIVE);
  loadingState$ = new BehaviorSubject<boolean>(false);
  todoList$ = new BehaviorSubject<Todo[]>([]);

  readonly displayedColumns: string[] = [
    'name',
    'description',
    'actions',
  ];

  constructor(private service: TodoService, private router: Router) {
    this.filterState$.pipe(
      tap((filter) => this.service.getFilteredTodoList(filter).pipe(
        tap(data => this.setTodoList(data))
      ).subscribe())
    ).subscribe();
  }

  setTodoList(todoList: Todo[]) {
    console.log(new Date().toISOString(), '- setTodoList:', todoList);
    this.todoList$.next(todoList);
  }

  setLoading(loadingState: boolean) {
    console.log(new Date().toISOString(), '- setLoading:', loadingState);
    this.loadingState$.next(loadingState);
  }

  toggleFilter(filterState: TodoStatus): void {
    console.log(new Date().toISOString(), '- toggleFilter:', filterState);
    this.filterState$.next(filterState);
  }

  goAddToDo(): void {
    this.router.navigate(['todos', 'add']);
  }
  goViewToDo(id: string): void {
    this.router.navigate(['todos', 'view', id]);
  }
  goEditToDo(id: string): void {
    this.router.navigate(['todos', 'edit', id]);
  }
  toggleDone(id: string): void {
    this.service.completeToDoStatus(id).subscribe();
  }
  toggleActive(id: string): void {
    this.service.activateToDoStatus(id).subscribe();
  }
  deleteToDo(id: string): void {
    this.service.deleteToDo(id).subscribe();
  }
}