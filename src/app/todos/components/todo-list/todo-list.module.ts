import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { TodoService } from '../../services/todo.service';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatSelectModule,
    MatTableModule,
    MatIconModule,
    MatButtonToggleModule,
    MatButtonModule,
  ],
  exports: [ ],
  providers: [
    TodoService,
  ]
})
export class TodoListModule { }
