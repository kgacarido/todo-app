import { TodoStatus } from "../services/todo.state"

export interface Todo {
    id: string,
    name: string,
    targetDate?: Date,
    description: string,
    status?: TodoStatus
}
