import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoAddComponent } from './components/todo-add/todo-add.component';
import { TodoEditComponent } from './components/todo-edit/todo-edit.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoViewComponent } from './components/todo-view/todo-view.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'todos',
  },
  {
    path: 'todos',
    component: TodoListComponent,
  },
  {
    path: 'todos/add',
    component: TodoAddComponent,
  },
  {
    path: 'todos/view/:id',
    component: TodoViewComponent,
  },
  {
    path: 'todos/edit/:id',
    component: TodoEditComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodoRoutingModule { }
