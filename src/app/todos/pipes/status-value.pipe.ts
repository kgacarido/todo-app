import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'statusValue' })
export class StatusValuePipe implements PipeTransform {

  transform(value: string): unknown {
    return "status_" + String(value).toLocaleLowerCase();
  }
}
