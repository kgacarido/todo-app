import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoRoutingModule } from './todo-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoListModule } from './components/todo-list/todo-list.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StatusValuePipe } from './pipes/status-value.pipe';
import { TodoAddComponent } from './components/todo-add/todo-add.component';
import { TodoViewComponent } from './components/todo-view/todo-view.component';
import { TodoEditComponent } from './components/todo-edit/todo-edit.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [
    TodoListComponent,
    StatusValuePipe,
    TodoAddComponent,
    TodoViewComponent,
    TodoEditComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatSelectModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    TodoRoutingModule,
    TodoListModule,
    //MatDividerModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
  ],
  exports: [
    TodoListComponent,
  ],
})
export class TodoModule { }
